[![pipeline status](https://gitlab.com/wpdesk/wp-show-decision/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-show-decision/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-show-decision/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-show-decision/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-show-decision/v/stable)](https://packagist.org/packages/wpdesk/wp-show-decision) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-show-decision/downloads)](https://packagist.org/packages/wpdesk/wp-show-decision) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-show-decision/v/unstable)](https://packagist.org/packages/wpdesk/wp-show-decision) 
[![License](https://poser.pugx.org/wpdesk/wp-show-decision/license)](https://packagist.org/packages/wpdesk/wp-show-decision)

WordPress Library for Decision whether to show something. 
===================================================
Idea is that you have ShouldShowStrategy interface with one bool method. Thanks to this you can create a class that
delegates decision whether to show something. Your class does not have to hardcode that decision.

## Requirements

PHP 5.6 or later.

## Composer

You can install the bindings via [Composer](http://getcomposer.org/). Run the following command:

```bash
composer require --dev wpdesk/wp-show-decision
```

To use the bindings, use Composer's [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading):

```php
require_once 'vendor/autoload.php';
```

## Usage

```php
    new Beacon(
        '6057086f-4b25-4e12-8735-fbc556d2dc01',
        new PostTypeStrategy('automation'),
        $this->get_plugin_assets_url()
```

```php
    if ( is_admin() ) {
        ( new RateNotices(
            [ new TwoWeeksNotice( $this->plugin_url . '/assets', new PostTypeStrategy([
                [ 'page' => 'x', 'name' => 'stefan' ], // show when page = x and name = stefan
                [ 'page' => 'y' ]  // OR when page = y
            ] ) ) ]
        ) )->hooks();
    }
```


## Project documentation

PHPDoc: https://wpdesk.gitlab.io/wp-show-decision/index.html  