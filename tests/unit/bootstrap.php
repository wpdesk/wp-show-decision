<?php
/**
 * PHPUnit bootstrap file
 */

require_once __DIR__ . '/../../vendor/autoload.php';

require_once __DIR__ . '/Stub/WP_Post.php';
require_once __DIR__ . '/Stub/WC_Shipping_Zones.php';
require_once __DIR__ . '/Stub/WC_Shipping_Method.php';

WP_Mock::setUsePatchwork( true );
WP_Mock::bootstrap();
