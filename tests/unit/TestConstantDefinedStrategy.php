<?php


namespace unit;

use WPDesk\ShowDecision\ConstantDefinedStrategy;

class TestConstantDefinedStrategy extends \WP_Mock\Tools\TestCase
{

    public function testShouldDisplayWhenConstantIsDefined()
    {
        // Given
        define('TEST_CONSTANT', 'test');
        $strategy = new ConstantDefinedStrategy('TEST_CONSTANT');

        // When & Then
        $this->assertTrue($strategy->shouldDisplay());
    }

    public function testShouldNotDisplayWhenConstantIsNotDefined()
    {
        // Given
        $strategy = new ConstantDefinedStrategy('TEST2_CONSTANT');

        // When & Then
        $this->assertFalse($strategy->shouldDisplay());
    }

}
