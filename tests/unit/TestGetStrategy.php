<?php

namespace unit;

use PHPUnit\Framework\TestCase;
use WPDesk\ShowDecision\GetStrategy;

class TestGetStrategy extends TestCase {
	const true_1 = [ 'a' => '1' ];
	const true_2 = [ 'b' => '2' ];
	const true_3 = [ 'c' => '3' ];

	const false_1 = [ 'x' => '1' ];
	const false_2 = [ 'y' => '2' ];

	/**
	 * Prepares $_GET with true clauses
	 */
	private function prepare_get() {
		$_GET = [
			self::true_1,
			self::true_2,
			self::true_3
		];
	}

	protected function setUp(): void {
		parent::setUp();
		$this->prepare_get();
	}

	public function testAndClausesFailure() {
		$strategy = new GetStrategy( [
			[
				self::true_1,
				self::false_1
			]
		] );
		$this->assertFalse( $strategy->shouldDisplay() );
	}

	public function testAndClausesSuccess() {
		$strategy = new GetStrategy( [
			[
				self::true_1,
				self::true_2,
				self::true_3
			]
		] );
		$this->assertTrue( $strategy->shouldDisplay() );
	}

	public function testOrClausesSuccess() {
		$strategy = new GetStrategy( [
			[
				self::true_1,
				self::false_1
			],
			[
				self::true_1,
				self::true_2
			]
		] );
		$this->assertTrue( $strategy->shouldDisplay() );
	}

	public function testOrClausesFailure() {
		$strategy = new GetStrategy( [
			[
				self::true_1,
				self::false_1
			],
			[
				self::true_1,
				self::true_2,
				self::true_3,
				self::false_1
			]
		] );
		$this->assertFalse( $strategy->shouldDisplay() );
	}

}
