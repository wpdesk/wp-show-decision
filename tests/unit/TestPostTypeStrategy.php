<?php

namespace unit;

use PHPUnit\Framework\TestCase;
use WPDesk\ShowDecision\PostTypeStrategy;


class TestPostTypeStrategy extends TestCase {
	const valid_post_type = 'test_post_type';
	const invalid_post_type = 'invalid';

	protected function setUp(): void {
		require_once __DIR__ . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'WP_Post.php';
		parent::setUp();
		\WP_Mock::setUp();
	}

	protected function tearDown(): void {
		parent::tearDown();
		\WP_Mock::tearDown();
	}

	public function testFailureWhenTriesUseInvalidPostType() {
		$strategy = new PostTypeStrategy( self::invalid_post_type );
		$this->assertFalse( $strategy->shouldDisplay() );
	}

	public function testValidPostTypeUsingGetSuccess() {
		$_GET     = [ 'post_type' => self::valid_post_type ];
		$strategy = new PostTypeStrategy( self::valid_post_type );
		$this->assertTrue( $strategy->shouldDisplay() );
	}

	public function testValidPostTypeUsingPostIdSuccess() {
		$post_id = 123;
		$_GET    = [
			'post' => $post_id
		];
		\WP_Mock::userFunction( 'get_post', [
			'times'  => 1,
			'return' => function () use ( $post_id ) {
				$post            = new \WP_Post();
				$post->ID        = $post_id;
				$post->post_type = self::valid_post_type;

				return $post;
			}
		] );

		$strategy = new PostTypeStrategy( self::valid_post_type );
		$this->assertTrue( $strategy->shouldDisplay() );
	}

}
