<?php

namespace unit\WooCommerce;


use WP_Mock\Tools\TestCase;
use WPDesk\ShowDecision\WooCommerce\ShippingMethodStrategy;

class TestShippingMethodStrategy extends TestCase
{

    public function testShouldReturnTrueWhenShipppingMethodMatches()
    {
        // Given
        $_GET =
            [ 'page' => 'wc-settings', 'tab' => 'shipping', 'section' => 'test_id' ]
        ;
        $strategy = new ShippingMethodStrategy('test_id');

        // When & Then
        $this->assertTrue($strategy->shouldDisplay());
    }

    public function testShouldReturnFalseWhenShipppingMethodDoesNotMatch()
    {
        // Given
        $_GET =
            [ 'page' => 'wc-settings', 'tab' => 'shipping', 'section' => 'test_id' ]
        ;
        $strategy = new ShippingMethodStrategy('other_test_id');

        // When & Then
        $this->assertFalse($strategy->shouldDisplay());
    }


}
