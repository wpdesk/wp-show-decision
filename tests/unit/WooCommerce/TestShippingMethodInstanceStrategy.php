<?php

namespace unit\WooCommerce;


use WP_Mock\Tools\TestCase;
use WPDesk\ShowDecision\WooCommerce\ShippingMethodInstanceStrategy;

class TestShippingMethodInstanceStrategy extends TestCase
{

    public function testShouldReturnTrueWhenShipppingMethodInstanceMatches()
    {
        // Expect
        \WP_Mock::passthruFunction('sanitize_key');

        // Given
        $_GET =
            [ 'page' => 'wc-settings', 'tab' => 'shipping', 'instance_id' => '1' ]
        ;
        $strategy = new ShippingMethodInstanceStrategy( new \WC_Shipping_Zones(), 'test_id');

        // When & Then
        $this->assertTrue($strategy->shouldDisplay());
    }

    public function testShouldReturnFalseWhenShipppingMethodInstanceDoesNotMatch()
    {
        // Expect
        \WP_Mock::passthruFunction('sanitize_key');

        // Given
        $_GET =
            [ 'page' => 'wc-settings', 'tab' => 'shipping', 'instance_id' => '2' ]
        ;
        $strategy = new ShippingMethodInstanceStrategy( new \WC_Shipping_Zones(), 'other_test_id');

        // When & Then
        $this->assertFalse($strategy->shouldDisplay());
    }


}
