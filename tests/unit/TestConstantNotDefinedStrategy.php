<?php


namespace unit;

use WPDesk\ShowDecision\ConstantNotDefinedStrategy;

class TestConstantNotDefinedStrategy extends \WP_Mock\Tools\TestCase
{

    public function testShouldNotDisplayWhenConstantIsDefined()
    {
        // Given
        define('TEST3_CONSTANT', 'test');
        $strategy = new ConstantNotDefinedStrategy('TEST_CONSTANT');

        // When & Then
        $this->assertFalse($strategy->shouldDisplay());
    }

    public function testShouldDisplayWhenConstantIsNotDefined()
    {
        // Given
        $strategy = new ConstantNotDefinedStrategy('TEST4_CONSTANT');

        // When & Then
        $this->assertTrue($strategy->shouldDisplay());
    }

}
