<?php


namespace unit;

use WPDesk\ShowDecision\AndStrategy;


class TestAndStrategy extends \WP_Mock\Tools\TestCase
{

    public function testShouldReturnTrueWhenAllConditionsAreTrue()
    {
        // Given
        $strategy1 = $this->createMock(\WPDesk\ShowDecision\ShouldShowStrategy::class);
        $strategy1->method('shouldDisplay')->willReturn(true);

        $strategy2 = $this->createMock(\WPDesk\ShowDecision\ShouldShowStrategy::class);
        $strategy2->method('shouldDisplay')->willReturn(true);

        $andStrategy = new AndStrategy($strategy1);
        $andStrategy->addCondition($strategy2);

        // When & Then
        $this->assertTrue($andStrategy->shouldDisplay());
    }

    public function testShouldReturnFalseWhenAnyConditionIsFalse()
    {
        // Given
        $strategy1 = $this->createMock(\WPDesk\ShowDecision\ShouldShowStrategy::class);
        $strategy1->method('shouldDisplay')->willReturn(true);

        $strategy2 = $this->createMock(\WPDesk\ShowDecision\ShouldShowStrategy::class);
        $strategy2->method('shouldDisplay')->willReturn(false);

        $andStrategy = new AndStrategy($strategy1);
        $andStrategy->addCondition($strategy2);

        // When & Then
        $this->assertFalse($andStrategy->shouldDisplay());
    }

}
