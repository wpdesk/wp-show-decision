<?php


namespace unit;

use WPDesk\ShowDecision\OrStrategy;

class TestOrStrategy extends \WP_Mock\Tools\TestCase
{

    public function testShouldReturnTrueWhenAnyConditionsAreTrue()
    {
        // Given
        $strategy1 = $this->createMock(\WPDesk\ShowDecision\ShouldShowStrategy::class);
        $strategy1->method('shouldDisplay')->willReturn(true);

        $strategy2 = $this->createMock(\WPDesk\ShowDecision\ShouldShowStrategy::class);
        $strategy2->method('shouldDisplay')->willReturn(false);

        $orStrategy = new OrStrategy($strategy1);
        $orStrategy->addCondition($strategy2);

        // When & Then
        $this->assertTrue($orStrategy->shouldDisplay());
    }

    public function testShouldReturnFalseWhenAllConditionsAreFalse()
    {
        // Given
        $strategy1 = $this->createMock(\WPDesk\ShowDecision\ShouldShowStrategy::class);
        $strategy1->method('shouldDisplay')->willReturn(false);

        $strategy2 = $this->createMock(\WPDesk\ShowDecision\ShouldShowStrategy::class);
        $strategy2->method('shouldDisplay')->willReturn(false);

        $orStrategy = new OrStrategy($strategy1);
        $orStrategy->addCondition($strategy2);

        // When & Then
        $this->assertFalse($orStrategy->shouldDisplay());
    }

}
